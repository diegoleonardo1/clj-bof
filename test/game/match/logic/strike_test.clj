(ns game.match.logic.strike-test
  (:require [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]
            [game.match.logic.strike :as strike]))

(def normal-range [16 71])
(def lucky-range [71 97])
(def critical-range [97 101])

(def config {:min-value    0
             :max-value    100
             :range-config {:normal   normal-range
                            :lucky    lucky-range
                            :critical critical-range}})

(defn matcher [type]
  {:strike-type  type
   :damage-point int?})

(deftest strike-point
  (let [power 50]
    (testing "Should return :normal matcher to all values between normal range"
      (doseq [v (range (first normal-range) (second normal-range))]
        (is (match? (matcher :normal)
                    (strike/strike-point config v power)))))
    (testing "Should return :lucky matcher to all values between lucky range"
      (doseq [v (range (first lucky-range) (second lucky-range))]
        (is (match? (matcher :lucky)
                    (strike/strike-point config v power)))))
    (testing "Should return :critical matcher to all values between critical range"
      (doseq [v (range (first critical-range) (second critical-range))]
        (is (match? (matcher :critical)
                    (strike/strike-point config v power)))))
    (testing "Should return :error matcher to all values between error range"
      (doseq [v (range (:min-value config) (first normal-range))]
        (is (match? (matcher :error)
                    (strike/strike-point config v power)))))))

(def damage-point (comp :damage-point strike/strike-point))

(deftest error-strike
  (testing "Should return 0 ever"
    (is (= 0 (damage-point config 10 10)))))

(deftest normal-strike
  (testing "Should return one third of the value ever"
    (is (= 5 (damage-point config 16 16)))
    (is (= 23 (damage-point config 16 70)))
    (is (= 930 (damage-point config 16 2790)))))

(deftest lucky-strike
  (testing "Should return twenty percent more than one-third ever"
    (is (= 29 (damage-point config 71 71)))
    (is (= 38 (damage-point config 71 96)))
    (is (= 36 (damage-point config 71 90)))))

(deftest critical-strike
  (testing "Should return the double of one-third ever"
    (is (= 64 (damage-point config 98 97)))
    (is (= 66 (damage-point config 98 100)))
    (is (= 66 (damage-point config 98 98)))))
