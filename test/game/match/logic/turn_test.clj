(ns game.match.logic.turn-test
  (:require [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]
            [game.adapters.player :as player]
            [game.match.logic.turn :as turn]
            [game.match.logic.mock-number-factory :as mock-number-factory]))

(def normal-range [16 71])
(def lucky-range [71 97])
(def critical-range [97 101])

(def config {:min-value    0
             :max-value    100
             :range-config {:normal   normal-range
                            :lucky    lucky-range
                            :critical critical-range}})

(def enemy-matcher {:name  string?
                    :hp    int?
                    :power int?})

(deftest turn
  (testing "Should return a map with striked-enemy, strike-type and damage-point"
    (let [mock-num-factory (mock-number-factory/->mock-number-factory 70)
          striker          (player/create-player "Player 1" 100 50)
          enemy            (player/create-player "Player 2" 100 50)
          result           (turn/turn config mock-num-factory striker enemy)]
      (is (match? {:striked-enemy {:hp 83}
                   :strike-type   :normal
                   :damage-point  17}
                  result))
      (is (match? enemy-matcher
                  (-> result
                      :striked-enemy))))))
