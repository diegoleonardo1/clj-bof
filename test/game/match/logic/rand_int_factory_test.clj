(ns game.match.logic.rand-int-factory-test
  (:require [clojure.test :refer [deftest testing is]]
            [mockfn.macros :as mfnm]
            [game.match.logic.lucky-number-factory :as lucky.number.factory]
            [game.match.logic.rand-int-factory :as rand.int.factory]))

(deftest lucky-number
  (let [lucky-number (rand.int.factory/->luck-number)]
    (testing "Should return zero when minimum value not reached"
      (mfnm/providing [(rand-int number?) 10]
                      (is (= 0
                             (lucky.number.factory/lucky-number lucky-number 15 100)))))

    (testing "Should return the random lucky number when it is over the minimum value"
      (let [expected 80]
        (mfnm/providing [(rand-int number?) expected]
                        (is (= expected
                               (lucky.number.factory/lucky-number lucky-number 15 100))))))))
