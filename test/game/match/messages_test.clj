(ns game.match.messages-test
  (:require [clojure.test :refer [deftest testing]]
            [game.utils.assertion :as assertion.util]
            [game.match.messages :as msgs]))

(deftest messages
  (testing "Should return the messages in portuguese as default language"
    (assertion.util/assertion "\nO jogo começou" (msgs/starting :pt-br))
    (assertion.util/assertion "Batalha entre John Doe e Joe Public\n" (msgs/info-battle :pt-br "John Doe" "Joe Public"))
    (assertion.util/assertion "Normal - 70 HP\n" (msgs/strike-type :pt-br :normal 70))
    (assertion.util/assertion "John Doe atacou Joe Public" (msgs/hit :pt-br "John Doe" "Joe Public")))

  (testing "Should return the messages in english as default language"
    (assertion.util/assertion "\nThe game has been started" (msgs/starting :en))
    (assertion.util/assertion "The battle between John Doe and Joe Public\n" (msgs/info-battle :en "John Doe" "Joe Public"))
    (assertion.util/assertion "Normal - 70 HP\n" (msgs/strike-type :en :normal 70))
    (assertion.util/assertion "John Doe has hit Joe Public" (msgs/hit :en "John Doe" "Joe Public"))))
