(ns game.match.do-play-test
  (:require [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]
            [game.match.do-play :as match]
            [game.diplomats.printers.mock-vector-printer :as mock.vector.printer]
            [game.match.logic.mock-number-factory :as mock.number.factory]
            [game.adapters.player :as player]))

(def printer-state (atom []))

(def dependencies {:env     :test
                   :lang    :en
                   :config  {:min-value    0
                             :max-value    100
                             :range-config {:normal   [16 71]
                                            :lucky    [71 97]
                                            :critical [97 101]}}
                   :printer (mock.vector.printer/->mock-printer printer-state)

                   :lucky-number-factory (mock.number.factory/->mock-number-factory 100)})

(def players [(player/create-player "John Doe" 100 100)
              (player/create-player "Joe Public" 1 1)])

(def expected-printed-messages ["\nThe game has been started"
                                "The battle between John Doe and Joe Public\n"
                                "John Doe has hit Joe Public"
                                "Critical! - 66 HP\n"])

(deftest start!
  (let [result (match/start! dependencies
                             (first players)
                             (second players))]
    (testing "Should play a game"
      (is (match? {:winner {:name  "John Doe"
                            :hp    100
                            :power 100}
                   :turn   1}
                  result)))
    (testing "Should print the messages of the game"
      (is (= expected-printed-messages
             @printer-state)))))
