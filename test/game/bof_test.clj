(ns game.bof-test
  (:require [clojure.test :refer [deftest testing is]]
            [game.bof :as m]
            [mockfn.macros :as mfnm]
            [game.configuration :as config]
            [game.versions.router :as version.router]
            [game.versions.simple.operator :as seq.version]
            [game.diplomats.printers.mock-vector-printer :as mock.vector.printer]
            [game.diplomats.readers.mock-reader :as mock.reader]
            [game.match.logic.mock-number-factory :as mock.number.factory]))

(deftest match-option-not-available
  (testing "Should print in the output the option not available message"
    (is (= "\nWhich version of the game would you like to play?\n1 (simple)\n2 (cli)\n\n[ERROR] Option does not exist in the system\n"
           (with-out-str (with-in-str "foobar" (m/-main "en")))))))

(defn- with-new-reader [deps reader]
  (assoc deps :reader reader))

(defn- dependencies []
  (-> (config/init-deps :en :test)
      (assoc :lucky-number-factory (mock.number.factory/->mock-number-factory 100))
      (assoc :printer (mock.vector.printer/->mock-printer (atom [])))))

(deftest main-simple-version-flow
  (let [{:keys [reader] :as deps} (dependencies)]
    (testing "Should play a match in a simple version"
      (mfnm/providing [(config/init-deps :en :test) deps
                       (version.router/which-version! :en reader) (str 1)
                       (seq.version/catch-player! deps :first) (let [new-deps (with-new-reader deps (mock.reader/->mock-reader "John 100 100"))]
                                                                 (seq.version/catch-player! new-deps :first))
                       (seq.version/catch-player! deps :second) (let [new-deps (with-new-reader deps (mock.reader/->mock-reader "Joe 1 1"))]
                                                                  (seq.version/catch-player! new-deps :second))]
                      (is (= "\nThe game is over, the winner is John with remaining HP 100\n"
                             (m/-main "en" "test")))))))
