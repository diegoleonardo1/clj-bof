(ns game.configuration-test
  (:require [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]
            [game.utils.assertion :as assertion.util]
            [game.configuration :as config]
            [game.diplomats.readers.stdin :as stdin.reader]
            [game.diplomats.printers.stdout :as stdout.printer]
            [game.match.logic.rand-int-factory :as rand.int.factory]))

(def config-matcher {:env    keyword?
                     :lang   keyword?
                     :config {:min-value    0
                              :max-value    100
                              :range-config {:normal   vector?
                                             :lucky    vector?
                                             :critical vector?}}})

(deftest init-deps
  (testing "Should return the production configuration"
    (let [config (config/init-deps :en :prod)]
      (is (match? config-matcher
                  config))
      (assertion.util/assertion :prod (:env config))
      (assertion.util/assertion :en (:lang config))
      (assertion.util/assertion (type (stdout.printer/->stdout-printer))
                                (type (:printer config)))
      (assertion.util/assertion (type (stdin.reader/->stdin-reader))
                                (type (:reader config)))
      (assertion.util/assertion (type (rand.int.factory/->luck-number))
                                (type (:lucky-number-factory config)))))

  (testing "Should return prd as default environment, and pt-br as default language"
    (let [{:keys [env lang]} (config/init-deps nil nil)]
      (assertion.util/assertion :prod env)
      (assertion.util/assertion :pt-br lang))))
