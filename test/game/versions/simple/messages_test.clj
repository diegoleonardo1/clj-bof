(ns game.versions.simple.messages-test
  (:require [clojure.test :refer [deftest testing]]
            [game.utils.assertion :as assertion.util]
            [game.versions.simple.messages :as msgs]))

(deftest messages
  (testing "Should return the messages in portuguese as default language"
    (assertion.util/assertion "Entre a primeira personagem: (e.g John Doe 100 50)"
                              (msgs/input-data :pt-br :first))
    (assertion.util/assertion "Entre a segunda personagem: (e.g John Doe 100 50)"
                              (msgs/input-data :pt-br :second))
    (assertion.util/assertion "\nVocê entrou com algum dado errado. Por favor, tente novamente.\n"
                              (msgs/wrong-input :pt-br))
    (assertion.util/assertion "\nJogo acabou, o vencedor foi John com HP restante de 100\n"
                              (msgs/final-match :pt-br "John" 100))
    (assertion.util/assertion "O número limite de tentativas foi atingido e o programa será encerrado"
                              (msgs/exceeded-attempts :pt-br)))

  (testing "Should return the messages in english as default language"
    (assertion.util/assertion "Do insert the first game character: (e.g John Doe 100 50)"
                              (msgs/input-data :en :first))
    (assertion.util/assertion "Do insert the second game character: (e.g John Doe 100 50)"
                              (msgs/input-data :en :second))
    (assertion.util/assertion "\nWrong data has been input. Please, try again.\n"
                              (msgs/wrong-input :en))
    (assertion.util/assertion "\nThe game is over, the winner is John with remaining HP 100\n"
                              (msgs/final-match :en "John" 100))
    (assertion.util/assertion "The limit number of attempts has been reached, and the program will finish"
                              (msgs/exceeded-attempts :en))))
