(ns game.versions.simple.operator-test
  (:require [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]
            [game.versions.simple.operator :as seq.version]
            [game.diplomats.printers.mock-printer :as mock.printer]
            [game.diplomats.printers.mock-vector-printer :as mock.vector.printer]
            [game.diplomats.readers.mock-reader :as mock.reader]
            [mockfn.macros :as mfnm]
            [game.match.logic.mock-number-factory :as mock.number.factory]
            [game.configuration :as config]))

(def player-matcher {:name  string?
                     :hp    int?
                     :power int?})

(defn- with-new-reader [deps reader]
  (assoc deps :reader reader))

(defn- printer-state [deps]
  (-> (:printer deps)
      :state
      deref))

(defn- execute-not-manage-fn [f]
  (try
    (f)
    (catch Exception _)))

(deftest catch-player!
  (let [deps {:lang :en
              :printer (mock.printer/->mock-printer (atom {}))}]
    (testing "Should return a map with fulfilled user data"
      (let [new-deps (with-new-reader deps (mock.reader/->mock-reader "'John Doe' 100 100"))]
        (is (match? player-matcher
                    (seq.version/catch-player! new-deps :first)))))

    (testing "Should trying the attempt limit number of times when inserting wrong data"
      (let [new-deps (with-new-reader deps (mock.reader/->mock-reader "John nil 100"))
            fn (partial seq.version/catch-player! new-deps :first)]
        (execute-not-manage-fn fn)
        (is (= seq.version/attempt-limit
               (-> (printer-state new-deps)
                   count)))))

    (testing "Should throw an exception after some input wrong data attempts"
      (let [new-deps (with-new-reader deps (mock.reader/->mock-reader "John nil"))]
        (is (thrown-with-msg? clojure.lang.ExceptionInfo
                              #"The limit number of attempts has been reached, and the program will finish"
                              (seq.version/catch-player! new-deps :first)))))))

(deftest execute!
  (let [deps (-> (config/init-deps :en :test)
                 (assoc :lucky-number-factory (mock.number.factory/->mock-number-factory 100)))]
    (testing "Should execute a match"
      (mfnm/providing [(seq.version/catch-player! deps :first) (let [new-deps (with-new-reader deps (mock.reader/->mock-reader "John 100 100"))]
                                                                 (seq.version/catch-player! new-deps :first))
                       (seq.version/catch-player! deps :second) (let [new-deps (with-new-reader deps (mock.reader/->mock-reader "Joe 1 1"))]
                                                                  (seq.version/catch-player! new-deps :second))]
                      (is (= "\nThe game is over, the winner is John with remaining HP 100\n"
                             (seq.version/execute! deps)))))

    (testing "Should catch exception and print the message"
      (let [new (assoc deps :printer (mock.vector.printer/->mock-printer (atom [])))]
        (seq.version/execute! new)
        (is (= "The limit number of attempts has been reached, and the program will finish"
               (-> (printer-state new)
                   last)))))))
