(ns game.versions.cli.operator-test
  (:require [clojure.test :refer [deftest testing is]]
            [matcher-combinators.test :refer [match?]]
            [game.configuration :as config]
            [game.versions.cli.operator :as cli.version]
            [game.match.logic.mock-number-factory :as mock.number.factory]
            [game.adapters.player :as player]
            [game.diplomats.printers.mock-printer :as mock.printer]
            [game.db.atom-database :as atom.database]
            [game.db.database :as database]))

(def add-player-matcher {:status  boolean?
                         :message string?})

(def printer-state (atom {}))

(defn deps [state] {:lang                 :en
                    :database             (atom.database/->atom-database (atom state))
                    :printer              (mock.printer/->mock-printer printer-state)
                    :lucky-number-factory (mock.number.factory/->mock-number-factory 100)})

(deftest add-player!
  (let [dependencies (deps {})
        player       (cli.version/add-player! dependencies "John" 100 100)]
    (testing "Should return a map after adding a player"
      (is (match? add-player-matcher
                  player)))

    (testing "Should add a player"
      (is (= 1
             (-> (database/all-players (:database dependencies))
                 count)))))

  (testing "Should not allow adding more than two players per match"
    (let [dependencies             (deps {:1 (player/create-player  "John" 100 100)
                                          :2 (player/create-player  "Joe" 100 100)})
          {:keys [status message]} (cli.version/add-player! dependencies "Michael" 100 100)]
      (is (false? status))
      (is (= "The number of allowed players has already been reached. Please, remove one of the players before adding a new one"
             message)))))

(deftest remove-player!
  (testing "Should remove one player"
    (let [{:keys [database] :as deps} (deps {:1 (player/create-player "John" 100 100)})
          {:keys [status message]} (cli.version/remove-player! deps  :1)]
      (is (= 0
             (-> (database/all-players database)
                 count)))
      (is (true? status))
      (is (= "The player with id :1 has been removed successfully"
             message)))))

(defn load-players [db]
  (database/insert-player db (player/create-player "John" 100 100))
  (database/insert-player db (player/create-player "Joe" 1 1)))

(deftest start!
  (testing "Should starting a match"
    (let [deps (-> (config/init-deps :en :test)
                   (assoc :lucky-number-factory (mock.number.factory/->mock-number-factory 100)))]
      (load-players (:database deps))
      (let [{:keys [status message]} (cli.version/start! deps)]
        (is (true? status))
        (is (= "\nThe game is over, the winner is John with remaining HP 100\n"
               message)))))

  (testing "Should not starting a match when the number of players is below two"
    (let [dependencies             (deps {:1 (player/create-player  "John" 100 100)})
          {:keys [status message]} (cli.version/start! dependencies)]
      (is (false? status))
      (is (= "The game needs two players to start. Please, enter the number of players and try again"
             message)))))
