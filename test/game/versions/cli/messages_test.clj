(ns game.versions.cli.messages-test
  (:require [clojure.test :refer [deftest testing]]
            [game.utils.assertion :as assertion.util]
            [game.versions.cli.messages :as msgs]))

(deftest messages
  (testing "Should return the messages in portuguese as default language"
    (assertion.util/assertion "O número de jogadores permitidos já foi alcançado. Por favor, remova um dos jogadores antes de adicionar um novo"
                              (msgs/limit-of-players-exceeded :pt-br))
    (assertion.util/assertion "O jogador com id :1 foi adicionado com sucesso"
                              (msgs/player-added :pt-br :1))
    (assertion.util/assertion "O jogador com id :1 foi removido com sucesso"
                              (msgs/player-removed :pt-br :1))
    (assertion.util/assertion "O jogo precisa de dois jogadores para começar. Por favor, insira o número de jogadores e tente novamente"
                              (msgs/player-below-of-the-minimum-to-play :pt-br))
    (assertion.util/assertion "\nJogo acabou, o vencedor foi John com HP restante de 100\n"
                              (msgs/final-match :pt-br "John" 100))
    (assertion.util/assertion "Adiciona um jogador para a partida"
                              (msgs/short-info {:cmd :add-player :lang :pt-br}))
    (assertion.util/assertion "Mostra os jogadores disponíveis para jogar"
                              (msgs/short-info {:cmd :show-players :lang :pt-br}))
    (assertion.util/assertion "Remove um jogador da partida"
                              (msgs/short-info {:cmd :remove-player :lang :pt-br}))
    (assertion.util/assertion "Começa a partida se o número obrigatório de jogadores por partida estiver disponível"
                              (msgs/short-info {:cmd :start :lang :pt-br}))
    (assertion.util/assertion "Informe um jogador no formato: name hp power (eg. \"John Doe\" 100 50)\nNOTA: Se desejar inserir um nome composto, faça-o usando aspas duplas"
                              (msgs/completion-hint {:cmd :add-player :lang :pt-br}))
    (assertion.util/assertion "Informe o ID como argumento no formato de keyword UUID. (eg. :bc244d4a-07b5-11ec-9a03-0242ac130003)"
                              (msgs/completion-hint {:cmd :remove-player :lang :pt-br})))

  (testing "Should return the messages in english as default language"
    (assertion.util/assertion "The number of allowed players has already been reached. Please, remove one of the players before adding a new one"
                              (msgs/limit-of-players-exceeded :en))
    (assertion.util/assertion "The player with id :1 has been added successfully"
                              (msgs/player-added :en :1))
    (assertion.util/assertion "The player with id :1 has been removed successfully"
                              (msgs/player-removed :en :1))
    (assertion.util/assertion "The game needs two players to start. Please, enter the number of players and try again"
                              (msgs/player-below-of-the-minimum-to-play :en))
    (assertion.util/assertion "\nThe game is over, the winner is John with remaining HP 100\n"
                              (msgs/final-match :en "John" 100))
    (assertion.util/assertion "Add a player to the game"
                              (msgs/short-info {:cmd :add-player :lang :en}))
    (assertion.util/assertion "Show the players available to starting the game"
                              (msgs/short-info {:cmd :show-players :lang :en}))
    (assertion.util/assertion "Remote a player from the match"
                              (msgs/short-info {:cmd :remove-player :lang :en}))
    (assertion.util/assertion "Start the game if it has two required players to play"
                              (msgs/short-info {:cmd :start :lang :en}))
    (assertion.util/assertion "Do insert a player in the format: name hp power (eg. \"John Doe\" 100 50)\nNOTE: If you would like to insert a composed name, please do it using double quotes"
                              (msgs/completion-hint {:cmd :add-player :lang :en}))
    (assertion.util/assertion "Do Inform ID as argument in format keyword UUID. (eg. :bc244d4a-07b5-11ec-9a03-0242ac130003)"
                              (msgs/completion-hint {:cmd :remove-player :lang :en}))))
