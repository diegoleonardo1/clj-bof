(ns game.versions.input-extractor-test
  (:require [clojure.test :refer [deftest testing is]]
            [game.versions.input-extractor :as input.extractor]))

(deftest info-from-input-data
  (testing "Should extract info from input data correctly"
    (let [[name [hp power]] (input.extractor/info-from-input-data "Joe Public 100 200 200 400")]
      (is (= "Joe Public" name))
      (is (= "100" hp))
      (is (= "200" power)))))
