(ns game.versions.router-test
  (:require [clojure.test :refer [deftest testing is]]
            [game.utils.assertion :as assertion.util]
            [game.diplomats.readers.mock-reader :as mock.reader]
            [game.diplomats.printers.mock-printer :as mock.printer]
            [game.versions.router :as router]
            [game.versions.messages :as msg.util]
            [game.versions.simple.operator :as simple.version]
            [game.versions.cli.operator :as cli.version]))

(deftest version-option
  (testing "Should return the version correctly"
    (assertion.util/assertion :simple (router/version-option "1"))
    (assertion.util/assertion :cli (router/version-option "2"))
    (assertion.util/assertion :error (router/version-option "foo"))))

(deftest which-version!
  (let [text-base "\nQual versão do aplicativo você deseja executar?\n1 (simples)\n2 (cli)\n"
        mock-reader (mock.reader/->mock-reader text-base)]
    (testing "Should return a similar message from util.message ns"
      (is text-base
          (router/which-version! :pt-br mock-reader)))))

(deftest choose!
  (testing "Should execute the simple version"
    (let [deps {:version :default :deps {:printer (mock.printer/->mock-printer (atom {}))}}]
      (with-redefs [simple.version/execute!         (fn [_] :simple-version)
                    cli.version/execute!            (fn [_] :cli-version)
                    msg.util/version-does-not-exist (fn [_] :default-version)]
        (assertion.util/assertion :simple-version (router/choose! {:version :simple}))
        (assertion.util/assertion :cli-version (router/choose! {:version :cli}))
        (assertion.util/assertion :default-version (router/choose! deps))))))
