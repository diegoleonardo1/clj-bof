(ns game.diplomats.readers.stdin-test
  (:require [clojure.test :refer [deftest testing is]]
            [game.diplomats.readers.input-data :as reader]
            [game.diplomats.readers.stdin :as stdin]))

(deftest read!
  (testing "Should read the input data correctly"
    (let [stdin-reader (stdin/->stdin-reader)]
      (is (= "John Doe"
             (with-in-str "John Doe" (reader/read! stdin-reader "What's your name?")))))))
