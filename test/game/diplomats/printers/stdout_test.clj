(ns game.diplomats.printers.stdout-test
  (:require [clojure.test :refer [deftest testing is]]
            [game.diplomats.printers.action :as printer]
            [game.diplomats.printers.stdout :as stdout]))

(deftest print!
  (testing "Should print the message passed to printer"
    (let [stdout-printer (stdout/->stdout-printer)]
      (is (= "Foobar\n"
             (with-out-str (printer/print! stdout-printer "Foobar")))))))
