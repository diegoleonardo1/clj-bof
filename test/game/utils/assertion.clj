(ns game.utils.assertion
  (:require  [clojure.test :refer [is]]))

(defn assertion [expected actual]
  (is (= expected actual)))
