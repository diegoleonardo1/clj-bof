(ns game.diplomats.printers.mock-vector-printer
  "This namespace has the mock record that implements the abstraction to
  print a game action message."
  (:require [game.diplomats.printers.action :as action]))

(defrecord mock-printer [^clojure.lang.Atom state]
  action/printer

  (print! [_ action]
    (swap! state conj action)
    action))
