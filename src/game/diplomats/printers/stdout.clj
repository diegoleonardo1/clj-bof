(ns game.diplomats.printers.stdout
  "This namespace has the record that implements the abstraction to
  print a game action message to device standard output."
  (:require [game.diplomats.printers.action :as action]))

(defrecord stdout-printer []
  action/printer

  (print! [_ action]
    (println action)))
