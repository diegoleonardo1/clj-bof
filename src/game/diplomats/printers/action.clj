(ns game.diplomats.printers.action)

(defprotocol printer
  "Abstraction to the action of print messages."

  (print! [this action] "Prints some action."))
