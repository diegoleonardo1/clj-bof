(ns game.diplomats.printers.mock-printer
  "This namespace has the mock record that implements the abstraction to
  print a game action message."
  (:require [game.diplomats.printers.action :as action])
  (:import java.util.UUID))

(defn- id []
  (str (UUID/randomUUID)))

(defrecord mock-printer [^clojure.lang.Atom state]
  action/printer

  (print! [_ action]
    (let [id (-> (id)
                 keyword)]
      (swap! state assoc id action)
      action)))
