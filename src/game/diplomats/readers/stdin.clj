(ns game.diplomats.readers.stdin
  "This namespace has the record that implements the abstraction to
  read data from standard input."
  (:require [game.diplomats.readers.input-data :as input-data]))

(defrecord stdin-reader []
  input-data/reader

  (read! [_ input]
    (println input)
    (flush)
    (read-line)))
