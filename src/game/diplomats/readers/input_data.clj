(ns game.diplomats.readers.input-data)

(defprotocol reader
  "Abstraction to the action of reading input data."

  (read! [this input] "Reads the input data."))
