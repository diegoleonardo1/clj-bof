(ns game.diplomats.readers.mock-reader
  "This namespace has the mock record that implements the abstraction to
  read data from standard input."
  (:require [game.diplomats.readers.input-data :as input-data]))

(defrecord mock-reader [response]
  input-data/reader

  (read! [this _]
    (:response this)))
