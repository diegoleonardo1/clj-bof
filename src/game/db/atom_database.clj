(ns game.db.atom-database
  "This namespace has the record that implements the database protocol."
  (:require [game.db.database :as db])
  (:import java.util.UUID))

(defn- id []
  (str (UUID/randomUUID)))

(defrecord atom-database [state]
  db/database

  (player-by [_ id]
    (id @state))

  (all-players [_]
    @state)

  (insert-player [_ data]
    (let [id (-> (id)
                 keyword)]
      (swap! state assoc id data)
      id))

  (remove-player [_ id]
    (swap! state dissoc id)
    id))
