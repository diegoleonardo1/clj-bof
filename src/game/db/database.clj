(ns game.db.database
  (:refer-clojure :exclude [get remove]))

(defprotocol database
  "Database abstration to store players."

  (player-by [this id] "Finds a player by identifier.")
  (all-players [this] "Show all the players.")
  (insert-player [this data] "Creates a player.")
  (remove-player [this id] "Removes a player."))
