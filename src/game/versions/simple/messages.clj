(ns game.versions.simple.messages
  "This namespace controls all the messages related to the game simple version interaction."
  (:require [taoensso.tempura :as tempura :refer [tr]]))

(def positions {:first  :config.position/first
                :second :config.position/second})

(def simple-version-dictionary
  {:pt-br
   {:config    {:input-data     "Entre a %1 personagem: (e.g John Doe 100 50)"
                :error-input    "\nVocê entrou com algum dado errado. Por favor, tente novamente.\n"
                :choose-version "\nQual versão do aplicativo você deseja executar?\n1 (simples)\n2 (cli)\n"
                :position       {:first  "primeira"
                                 :second "segunda"}}
    :game      {:end "\nJogo acabou, o vencedor foi %1 com HP restante de %2\n"}
    :exception {:exceeded-attempts "O número limite de tentativas foi atingido e o programa será encerrado"}}

   :en
   {:config    {:input-data     "Do insert the %1 game character: (e.g John Doe 100 50)"
                :error-input    "\nWrong data has been input. Please, try again.\n"
                :choose-version "\nWhich version of the game would you like to play?\n1 (simple)\n2 (cli)\n"
                :position       {:first  "first"
                                 :second "second"}}
    :game      {:end "\nThe game is over, the winner is %1 with remaining HP %2\n"}
    :exception {:exceeded-attempts "The limit number of attempts has been reached, and the program will finish"}}})

(def print-msg (partial tr {:dict simple-version-dictionary}))

(defn input-data [lang position]
  (let [position-player (position positions)
        pos (print-msg [lang] [position-player])]
    (print-msg [lang] [:config/input-data] [pos])))

(defn wrong-input [lang]
  (print-msg [lang] [:config/error-input]))

(defn final-match [lang winner-name winner-energy]
  (print-msg [lang] [:game/end] [winner-name winner-energy]))

(defn exceeded-attempts [lang]
  (print-msg [lang] [:exception/exceeded-attempts]))
