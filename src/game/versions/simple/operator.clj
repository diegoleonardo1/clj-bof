(ns game.versions.simple.operator
  (:require [game.match.do-play :as match]
            [game.adapters.player :as player]
            [game.versions.simple.messages :as msgs]
            [game.diplomats.printers.action :as action.printer]
            [game.diplomats.readers.input-data :as data.reader]
            [game.versions.input-extractor :as input.extractor]))

(def attempt-limit 3)

(defn catch-player!
  "Catches the player that has been input by the reader component.
  It throws an exception when the data is input wrongly for more time than the value
  configured in the `attempt-limit` symbol.

  It takes `deps` map as first argument. If it feels like it.
  See also [[init-deps]] inside configuration file.
  The other two arguments are `position` (:first or :second) and attempt (number)."
  ([deps position] (catch-player! deps position 0))
  ([{:keys [lang reader printer] :as deps} position attempt]
   (try
     (let [input-data        (data.reader/read! reader (msgs/input-data lang position))
           [name [hp power]] (input.extractor/info-from-input-data input-data)]
       (player/create-player name
                             (Integer/parseInt hp)
                             (Integer/parseInt power)))
     (catch Exception e
       (if (= attempt attempt-limit)
         (throw (ex-info (msgs/exceeded-attempts lang) {:error e}))
         (do (action.printer/print! printer (msgs/wrong-input lang))
             (catch-player! deps position (inc attempt))))))))

(defn execute!
  "Starts the game simple version.
  It takes `deps` map as an argument. If it feels like it.
  See also [[init-deps]] inside configuration file."
  [{:keys [printer lang] :as deps}]
  (try
    (let [player-one       (catch-player! deps :first)
          player-two       (catch-player! deps :second)
          {:keys [winner]} (match/start! deps player-one player-two)]
      (action.printer/print! printer
                             (msgs/final-match lang
                                               (:name winner)
                                               (:hp winner))))
    (catch Exception e
      (action.printer/print! printer (ex-message e)))))
