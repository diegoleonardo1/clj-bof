(ns game.versions.input-extractor
  (:require [clojure.string :as str])
  (:refer-clojure :exclude [name]))

(defn- name []
  (fn [s]
    (-> (str/split s #"[0-9]")
        first
        (str/replace #"'" "")
        (str/triml)
        (str/trimr))))

(defn- hp-power []
  (fn [s]
    (let [num      (re-seq #"\s\d+" s)
          hp-power ((juxt first second) num)]
      (map str/trim hp-power))))

(defn info-from-input-data
  "Extracts info (such as name, hp, and power) from the data.
  It takes `data`(string) as an argument.
  The data must be into format [name hp power] (e.g \"John Doe 100 100\")."
  [data]
  ((juxt (name)
         (hp-power))
   data))
