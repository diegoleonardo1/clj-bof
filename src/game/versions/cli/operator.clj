(ns game.versions.cli.operator
  "This namespace controls all interaction logic in the game CLI Version."
  (:require [cli4clj.cli :as cli]
            [game.versions.cli.messages :as msgs]
            [game.match.do-play :as match]
            [game.diplomats.printers.action :as action.printer]
            [game.adapters.player :as player]
            [game.db.database :as database]))

(def players-allowed 2)

(defn status-msg [status msg]
  {:status  status
   :message msg})

(defn- allowed-to-insert? [database]
  (let [count-players (-> (database/all-players database)
                          count)]
    (< count-players players-allowed)))

(defn add-player!
  "Add a player to the game database.
  It takes `deps` map as the first argument. If it feels like it.
  See also [[init-deps]] inside configuration file.
  It also takes the name (string), hp (integer), and power (integer) as the lasts arguments."
  [{:keys [database lang]} name hp power]
  (if (allowed-to-insert? database)
    (->> (player/create-player name hp power)
         (database/insert-player database)
         (msgs/player-added lang)
         (status-msg true))
    (status-msg false (msgs/limit-of-players-exceeded lang))))

(defn show-players!
  "Show players in the database.
  It takes `x` like `game.db.database` as an argument."
  [database]
  (database/all-players database))

(defn remove-player!
  "Removes a player from the game database.
  It takes `deps` map as the first argument. If it feels like it.
  See also [[init-deps]] inside configuration file.
  It also takes the id (string) as the second argument."
  [{:keys [database lang]} id]
  (->> (database/remove-player database id)
       (msgs/player-removed lang)
       (status-msg true)))

(defn- allowed-to-start? [players]
  (= (count players) players-allowed))

(defn start!
  "Has the logic to start a match.
  It takes `deps` map as an argument. If it feels like it.
  See also [[init-deps]] inside configuration file."
  [{:keys [database lang] :as deps}]
  (let [players (database/all-players database)]
    (if (allowed-to-start? players)
      (let [players          (vals players)
            player-one       (first players)
            player-two       (second players)
            {:keys [winner]} (match/start! deps player-one player-two)]
        (status-msg true (msgs/final-match lang
                                           (:name winner)
                                           (:hp winner))))
      (status-msg false (msgs/player-below-of-the-minimum-to-play lang)))))

(defn execute!
  "Starts the game CLI version.
  It takes `deps` map as an argument. If it feels like it.
  See also [[init-deps]] inside configuration file."
  [{:keys [database lang printer] :as deps}]
  (cli/start-cli {:cmds {:add-player {:fn              (fn [name hp power]
                                                         (->> (add-player! deps name hp power)
                                                              :message
                                                              (action.printer/print! printer)))
                                      :short-info      (msgs/short-info {:cmd  :add-player
                                                                         :lang lang})
                                      :completion-hint (msgs/completion-hint {:cmd  :add-player
                                                                              :lang lang})}
                         :ap         :add-player

                         :show-players {:fn         (fn []
                                                      (->> (show-players! database)
                                                           (action.printer/print! printer)))
                                        :short-info (msgs/short-info {:cmd  :show-players
                                                                      :lang lang})}
                         :sp           :show-players

                         :remove-player {:fn              (fn [id]
                                                            (->> (remove-player! deps id)
                                                                 :message
                                                                 (action.printer/print! printer )))
                                         :short-info      (msgs/short-info {:cmd  :remove-player
                                                                            :lang lang})
                                         :completion-hint (msgs/completion-hint {:cmd  :remove-player
                                                                                 :lang lang})}
                         :rp            :remove-player

                         :start {:fn         (fn []
                                               (->> (start! deps)
                                                    :message
                                                    (action.printer/print! printer)))
                                 :short-info (msgs/short-info {:cmd  :start
                                                               :lang lang})}
                         :st    :start}

                  :allow-eval          true
                  :prompt-string       "bof# "
                  :alternate-scrolling (some #(= % "alt") "alt")
                  :alternate-height    3}))
