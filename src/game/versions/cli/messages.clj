(ns game.versions.cli.messages
  "This namespace controls all the messages related to the game cli version interaction."
  (:require [taoensso.tempura :as tempura :refer [tr]]))

(def cli-game-dictionary
  {:pt-br
   {:player-added            "O jogador com id %1 foi adicionado com sucesso"
    :player-removed          "O jogador com id %1 foi removido com sucesso"
    :limit-players-exceeded  "O número de jogadores permitidos já foi alcançado. Por favor, remova um dos jogadores antes de adicionar um novo"
    :minimum-players-to-play "O jogo precisa de dois jogadores para começar. Por favor, insira o número de jogadores e tente novamente"
    :short-info              {:add-player    "Adiciona um jogador para a partida"
                              :show-players  "Mostra os jogadores disponíveis para jogar"
                              :remove-player "Remove um jogador da partida"
                              :start         "Começa a partida se o número obrigatório de jogadores por partida estiver disponível"}
    :completion-hint         {:add-player    "Informe um jogador no formato: name hp power (eg. \"John Doe\" 100 50)\nNOTA: Se desejar inserir um nome composto, faça-o usando aspas duplas"
                              :remove-player "Informe o ID como argumento no formato de keyword UUID. (eg. :bc244d4a-07b5-11ec-9a03-0242ac130003)"}
    :game                    {:end "\nJogo acabou, o vencedor foi %1 com HP restante de %2\n"}}

   :en
   {:player-added            "The player with id %1 has been added successfully"
    :player-removed          "The player with id %1 has been removed successfully"
    :limit-players-exceeded  "The number of allowed players has already been reached. Please, remove one of the players before adding a new one"
    :minimum-players-to-play "The game needs two players to start. Please, enter the number of players and try again"
    :short-info              {:add-player    "Add a player to the game"
                              :show-players  "Show the players available to starting the game"
                              :remove-player "Remote a player from the match"
                              :start         "Start the game if it has two required players to play"}
    :completion-hint         {:add-player    "Do insert a player in the format: name hp power (eg. \"John Doe\" 100 50)\nNOTE: If you would like to insert a composed name, please do it using double quotes"
                              :remove-player "Do Inform ID as argument in format keyword UUID. (eg. :bc244d4a-07b5-11ec-9a03-0242ac130003)"}
    :game                    {:end "\nThe game is over, the winner is %1 with remaining HP %2\n"}}})

(def print-msg (partial tr {:dict cli-game-dictionary}))

(defn limit-of-players-exceeded [lang]
  (print-msg [lang] [:limit-players-exceeded]))

(defn player-added [lang id]
  (print-msg [lang] [:player-added] [id]))

(defn player-removed [lang id]
  (print-msg [lang] [:player-removed] [id]))

(defn player-below-of-the-minimum-to-play [lang]
  (print-msg [lang] [:minimum-players-to-play]))

(defn final-match [lang winner-name winner-energy]
  (print-msg [lang] [:game/end] [winner-name winner-energy]))

(defmulti short-info :cmd)

(defmethod short-info :add-player [{:keys [lang]}]
  (print-msg [lang] [:short-info/add-player]))

(defmethod short-info :show-players [{:keys [lang]}]
  (print-msg [lang] [:short-info/show-players]))

(defmethod short-info :remove-player [{:keys [lang]}]
  (print-msg [lang] [:short-info/remove-player]))

(defmethod short-info :start [{:keys [lang]}]
  (print-msg [lang] [:short-info/start]))

(defmulti completion-hint :cmd)

(defmethod completion-hint :add-player [{:keys [lang]}]
  (print-msg [lang] [:completion-hint/add-player]))

(defmethod completion-hint :remove-player [{:keys [lang]}]
  (print-msg [lang] [:completion-hint/remove-player]))
