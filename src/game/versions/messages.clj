(ns game.versions.messages
  "This namespace controls all the messages to interaction related to choosing a version to execute."
  (:require [taoensso.tempura :as tempura :refer [tr]]))

(def version-dictionary
  {:pt-br
   {:config {:choose-version         "\nQual versão do aplicativo você deseja executar?\n1 (simples)\n2 (cli)\n"
             :version-does-not-exist "[ERRO] Opção não existe no sistema"}}

   :en
   {:config {:choose-version         "\nWhich version of the game would you like to play?\n1 (simple)\n2 (cli)\n"
             :version-does-not-exist "[ERROR] Option does not exist in the system"}}})

(def print-msg (partial tr {:dict version-dictionary}))

(defn choose-version [lang]
  (print-msg [lang] [:config/choose-version]))

(defn version-does-not-exist [lang]
  (print-msg [lang] [:config/version-does-not-exist]))
