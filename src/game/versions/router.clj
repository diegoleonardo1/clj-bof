(ns game.versions.router
  (:require [game.versions.messages :as msgs]
            [game.versions.simple.operator :as simple.version]
            [game.versions.cli.operator :as cli.version]
            [game.diplomats.readers.input-data :as data.reader]
            [game.diplomats.printers.action :as action.printer]))

(defn- choosed-option [option]
  (cond
    (= option "1") :simple
    (= option "2") :cli
    :else          :error))

(defn which-version!
  "Reader to get which version of the system run.

  It takes the lang (:pt-br or :en) as the first argument
  and any component that implements the `game.diplomats.reader.input-data`
  as the second argument."
  [lang reader]
  (->> (msgs/choose-version lang)
       (data.reader/read! reader)))

(defn version-option [option]
  (choosed-option option))

(defmulti choose!
  "Route which version of system will run based on keyword
  It takes a map with keys :version (:simple or :cli) and :deps map as an argument.

  If any version will match, It executes the :default multimethod."
  :version)

(defmethod choose! :simple [{:keys [deps]}]
  (simple.version/execute! deps))

(defmethod choose! :cli [{:keys [deps]}]
  (cli.version/execute! deps))

(defmethod choose! :default [{:keys [deps]}]
  (->> (msgs/version-does-not-exist (:lang deps))
       (action.printer/print! (:printer deps))))
