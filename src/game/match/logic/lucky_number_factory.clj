(ns game.match.logic.lucky-number-factory)

(defprotocol lucky-number-factory
  "Abstraction to be implemented by any component that generates random numbers."

  (lucky-number [this min max]
    "It generates a random number between min and max value."))
