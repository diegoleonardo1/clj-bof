(ns game.match.logic.mock-number-factory
  "This namespace has the mock record that implements the abstraction to
  generate a random number."
  (:require [game.match.logic.lucky-number-factory :as lnf]))

(defrecord mock-number-factory [mock-value]
  lnf/lucky-number-factory

  (lucky-number [_ _ _]
    mock-value))
