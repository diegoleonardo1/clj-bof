(ns game.match.logic.strike
  "This namespace has the functions that hold the logic
   behind the strike points inside the battle."
  (:require [clojure.spec.alpha :as spec]))

(defn- normal-strike [value]
  (-> (/ value 3)
      double
      Math/round))

(defn- lucky-strike [value]
  (let [normal-point (normal-strike value)]
    (-> (* normal-point 20)
        (/ 100)
        double
        Math/round
        (+ normal-point))))

(defn- critical-strike [value]
  (-> (normal-strike value)
      (* 2)))

(defn- in-range? [[min max] value]
  (spec/int-in-range? min max value))

(defn- type-of-strike [{:keys [range-config]} value]
  (cond
    (in-range? (:normal range-config) value)   :normal
    (in-range? (:lucky range-config) value)    :lucky
    (in-range? (:critical range-config) value) :critical
    :else                                      :error))

(defn- strike-point-return [type points]
  {:strike-type type
   :damage-point points})

(defmulti strike-point (fn [config lucky-number _]
                         (type-of-strike config lucky-number)))

(defmethod strike-point :normal [_ _ value]
  (->> (normal-strike value)
       (strike-point-return :normal)))

(defmethod strike-point :lucky [_ _ value]
  (->> (lucky-strike value)
       (strike-point-return :lucky)))

(defmethod strike-point :critical [_ _ value]
  (->> (critical-strike value)
       (strike-point-return :critical)))

(defmethod strike-point :default [_ _ _]
  (strike-point-return :error 0))
