(ns game.match.logic.rand-int-factory
  "This namespace has the record that implements the abstraction to
  generate a random number."
  (:require [game.match.logic.lucky-number-factory :as lnf]))

(defrecord luck-number []
  lnf/lucky-number-factory

  (lucky-number [_ min max]
    (let [number (rand-int max)]
      (if (> number min)
        number
        0))))
