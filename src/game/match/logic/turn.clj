(ns game.match.logic.turn
  (:require [game.match.logic.strike :as strike]
            [game.match.logic.lucky-number-factory :as lucky.factory]))

(defn turn
  "A function that controls the turn action logic in the battle.
  It takes `config` map as an argument. If it feels like it.
  See also [[init-deps]] inside configuration file."
  [{:keys [min-value max-value] :as config} lucky-number-factory striker enemy]
  (let [lucky-factor-number (lucky.factory/lucky-number lucky-number-factory min-value max-value)
        strike-point-result (strike/strike-point config lucky-factor-number (:power striker))
        remaining-hp (- (:hp enemy) (:damage-point strike-point-result))]
    (merge strike-point-result
           {:striked-enemy (assoc enemy :hp remaining-hp)})))
