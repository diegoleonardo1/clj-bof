(ns game.match.messages
  "This namespace controls all the messages related to a match inside the game."
  (:require [taoensso.tempura :as tempura :refer [tr]]))

(def game-dictionary
  {:any
   {:message {:strike-type "%1 - %2 HP\n"}}

   :pt-br
   {:game {:start       "\nO jogo começou"
           :introducing "Batalha entre %1 e %2\n"
           :hit         "%1 atacou %2"}

    :strike      {:type {:normal   "Normal"
                         :lucky    "Sorte!!!"
                         :critical "Crítico!"
                         :error    "Errou"}}
    :strike-copy :pt-br.strike/type}

   :en
   {:missing ":en missing text"
    :game    {:start       "\nThe game has been started"
              :introducing "The battle between %1 and %2\n"
              :hit         "%1 has hit %2"}

    :strike {:type {:normal   "Normal"
                    :lucky    "Lucky!!!"
                    :critical "Critical!"
                    :error    "Missed out"}}

    :strike-copy :en.strike/type}})

(def strike-types {:normal   :strike.type/normal
                   :lucky    :strike.type/lucky
                   :critical :strike.type/critical
                   :error    :strike.type/error})

(def print-msg (partial tr {:dict game-dictionary}))

(defn starting [lang]
  (print-msg [lang] [:game/start]))

(defn info-battle [lang name-player-1 name-player-2]
  (print-msg [lang] [:game/introducing] [name-player-1 name-player-2]))

(defn hit [lang striker-name enemy-name]
  (print-msg [lang] [:game/hit] [striker-name enemy-name]))

(defn strike-type [lang strike-type damage-point]
  (let [type           (strike-type strike-types)
        kind-of-damage (print-msg [lang] [type])]
    (print-msg [:any] [:message/strike-type] [kind-of-damage damage-point])))
