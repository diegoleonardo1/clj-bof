(ns game.match.do-play
  (:require [game.match.messages :as msgs]
            [game.match.logic.turn :as turn]
            [game.diplomats.printers.action :as action.printer]))

(defn- game-over? [hp]
  (true? (<= hp 0)))

(defn- do-battle!
  ([dependencies striker enemy] (do-battle! dependencies striker enemy 1))
  ([{:keys [lang printer config lucky-number-factory] :as deps} striker enemy turn]
   (action.printer/print! printer (msgs/hit lang (:name striker) (:name enemy)))
   (let [{:keys [striked-enemy strike-type damage-point]} (turn/turn config lucky-number-factory striker enemy)]
     (action.printer/print! printer (msgs/strike-type lang strike-type damage-point))
     (if (game-over? (:hp striked-enemy))
       {:winner striker
        :turn   turn}
       (do-battle! deps striked-enemy striker (inc turn))))))

(defn start!
  "Starts a battle.

  It takes `deps` map as the first argument. If it feels like it.
  See also [[init-deps]] inside the `game.configuration`.

  It also takes the players as lasts arguments. If it feels like it.
  See all `game.adapters.player`."
  [{:keys [lang printer] :as dependencies} player-1 player-2]
  (action.printer/print! printer (msgs/starting lang))
  (action.printer/print! printer (msgs/info-battle lang (:name player-1) (:name player-2)))
  (let [{:keys [winner turn]} (do-battle! dependencies player-1 player-2)]
    {:winner winner
     :turn   turn}))
