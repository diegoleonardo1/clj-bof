(ns game.bof
  (:require [game.configuration :as config]
            [game.versions.router :as version.router])
  (:gen-class))

(defn -main
  [& args]
  (let [lang    (keyword (first args))
        env     (keyword (second args))
        config  (config/init-deps lang env)
        option  (version.router/which-version! (:lang config) (:reader config))
        version (version.router/version-option option)]
    (version.router/choose! {:version version
                             :deps    config})))
