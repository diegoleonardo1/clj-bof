(ns game.adapters.player)

(defn create-player
  "Generates the schema model that represents a player.
  It takes `name` (string), `hp` (integer), and `power` (integer) as arguments."
  [name hp power]
  {:name  name
   :hp    hp
   :power power})
