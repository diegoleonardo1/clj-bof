(ns game.configuration
  (:require [game.diplomats.readers.stdin :as reader]
            [game.diplomats.readers.mock-reader :as mock.reader]
            [game.diplomats.printers.stdout :as printer]
            [game.diplomats.printers.mock-printer :as mock.printer]
            [game.match.logic.rand-int-factory :as lucky.number.factory]
            [game.db.atom-database :as database]))

(defn- environments []
  #{:prod :test})

(defn- languages []
  #{:pt-br :en})

(defn- environment [env]
  (-> (environments)
      (get env :prod)))

(defn- language [lang]
  (-> (languages)
      (get lang :pt-br)))

(defn- production? [env]
  (true? (= :prod (environment env))))

(defn init-deps
  "It generates all the configuration and dependencies necessary to start a match"
  [lang env]
  {:env     (environment env)
   :lang    (language lang)
   :config  {:min-value    0
             :max-value    100
             :range-config {:normal   [16 71]
                            :lucky    [71 97]
                            :critical [97 101]}}
   :printer (if (production? env)
                           (printer/->stdout-printer)
                           (mock.printer/->mock-printer (atom {})))

   :reader (if (production? env)
                           (reader/->stdin-reader)
                           (mock.reader/->mock-reader ""))

   :database             (database/->atom-database (atom {}))
   :lucky-number-factory (lucky.number.factory/->luck-number)})
