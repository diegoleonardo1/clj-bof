# Breath of Fantasy (bof)

Breath of Fantasy (BOF) is just a simple turn-based battle game.

![enter image description here](https://i1.wp.com/superchartisland.com/wp-content/uploads/2020/09/prfight.gif?resize=428,416&ssl=1)

But of course, much more simple than that classical game shown above (nostalgic feelings).

Briefly, in the Breath of Fantasy, you can insert two players and put them to battle.

In the battle, in each turn, is generated a random number and the calculation of the damage's weight is made.

During the match, all the actions will be printed on the standard output.

There are two ways to interact with the game:
- Simple: Your interaction is simpler. You insert two players through standard input, and the game will start automatically;

- CLI: Your experience is richer, and you could interact with the game through functions in a CLI tools fashion (such as you do use Unix terminal);

You will be able to choose one of the interaction options shown above after running the application.

It is worth saying which this game has support for two languages.
- PT-BR (default)
- English

I hope you enjoy the game!

## Installation
First of all, you must have Java and Clojure installed on your machine.

You might follow the installation guide of the languages ([Java](https://www.java.com/en/download/help/index_installing.html) and [Clojure](https://clojure.org/guides/getting_started)).

In my humble opinion, to make life easier, you might use [asdf](https://asdf-vm.com/) to do that.

After that, you can download (or clone) the project.

## Before go-ahead
Before running commands (and deep-dive through the code as well), maybe it will be helpful to know a bit of Clojure.

To help you in this journey, I will let [this gift](https://clojure.org/guides/learn/syntax) to you, little Padawan. =)

## Usage

So, with all the essential previous information said, let's go to the most awesome part. Of course, I am talking about run some commands. \o/

Inside the project's directory that you just downloaded, run the following commands:

1.Run the project directly:

    $ clojure -m game.bof

![Imgur](https://i.imgur.com/xvX80Fe.gif)

2.Run the project, overriding the default language (pt-br) to English (en).

    $ clojure -m game.bof en

![Imgur](https://i.imgur.com/U6E4b2l.gif)

3.Run the project's tests:

    $ clojure -M:test:runner

![Imgur](https://i.imgur.com/cMoPWjp.gif)

4.Build an uberjar:

    $ clojure -X:uberjar

![Imgur](https://i.imgur.com/ias7dEU.gif)

This will update the generated `pom.xml` file to keep the dependencies synchronized with
your `deps.edn` file. You can update the version (and SCM tag) information in the `pom.xml` using the
`:version` argument:

    $ clojure -X:uberjar :version '"1.2.3"'

If you don't want the `pom.xml` file in your project, you can remove it, but you will
also need to remove `:sync-pom true` from the `deps.edn` file (in the `:exec-args` for `depstar`).

6.Run that uberjar:

    $ java -jar bof.jar

![Imgur](https://i.imgur.com/xYVqSj8.gif)

## Options

- Language: Portuguese (pt-br) and English (en)
- Environments: Production (prd) and Test (test)

## Examples

### Simple Version:

![Imgur](https://i.imgur.com/fGK27L7.gif)
### Cli Version:

![Imgur](https://i.imgur.com/04Fhw33.gif)


### Bugs

We have some problems with input data at all.

Simple Version: When we try to insert data with a name concatenated to hp (such as "John Doe100 200"), the input-data extractor component ignores the first number and gets the next occurrences. Anyway, this component needs an improvement and more tests as well.

Cli Version: We just be able to insert composed names putting the double quotes. I suppose that decreases the user experience.

## License

Copyright © 2021 Diego Santos
