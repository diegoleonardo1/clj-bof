Project structure\
Date: 2021-09-02

Status:\
Proposed

Context:\
For the application to grow healthy, we needed a few standards to have maintainability and extensibility.

Decision:\
For simplicity's purpose, this project follows a structure more componentized, grouping its files by domains.

I suppose It will clearly express the project's purpose, as well as, will become make the expansion, and maintenance easy.

Consequences:
1. This change will make it easier to onboard new devs;
