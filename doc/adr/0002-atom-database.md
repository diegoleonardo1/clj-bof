Atom-db\
Date: 2021-09-02

Status:\
Proposed

Context:\
Based on the necessity to have some data persisted in a CLI Version (and maybe in the Simple version too), we need some database to hold this data.

Decision:\
It will use the simple Atom DB to hold the players' data.

Consequences:
1. This decision avoids any premature complexity regarding data storage;
2. It will use a Clojure native data structure, which suits well to the requirements;
3. It will help to improve the application performance;
